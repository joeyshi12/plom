.. Plom documentation
   Copyright 2021-2022 Colin B. Macdonald
   SPDX-License-Identifier: AGPL-3.0-or-later

Understanding the code
======================

The Plom code is organized into several main modules.
The primary modules are:


Plom Client
-----------

.. automodule:: plom.client
    :members:
.. automodule:: plom.client.annotator
    :members:


Plom Server
-----------

.. automodule:: plom.server
    :members:


Tools for producing papers, scanning papers, and finishing the grading process.

.. automodule:: plom.create
    :members:

.. automodule:: plom.scan
    :members:

.. automodule:: plom.finish
    :members:


Other supporting code
---------------------

.. automodule:: plom.db

.. automodule:: plom.manager

.. automodule:: plom.create.scribble_utils
    :members:

TeX Tools
---------

.. automodule:: plom.textools
    :members:

