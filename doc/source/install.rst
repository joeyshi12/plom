.. Copyright 2020 Colin B. Macdonald
   SPDX-License-Identifier: AGPL-3.0-or-later

Installing Plom from Source
===========================

.. toctree::
   :maxdepth: 2

   linux_installation.md
   macos_installation.md
   wsl_installation.md
